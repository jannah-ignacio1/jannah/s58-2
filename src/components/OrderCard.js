import React, { useState, Fragment, useEffect } from 'react';
import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Order({orderProp}){
	
	const{ purchasedOn, products } = orderProp
	const [pizza, setPizza] = useState([])

	useEffect(()=>{
		setPizza(products.map(pizza => {
			return (
				<Row>
					<Col>
						<Card className='p-3'>
							<Card.Body>
							<Card.Title>{ pizza.name }</Card.Title>
						  		<Card.Subtitle>quantity:</Card.Subtitle>
						   	 	<Card.Text>{ pizza.quantity }</Card.Text>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			)
		}))		
	}, [products])


return(
		<Fragment>
		<Container className="my-5 vh-50 d-inline-flex justify-content-center">
		
			<Col className="col-lg-5">
				<Card className='p-3'>
				 	<Card.Body>
				 		<Card.Title>
				 		{pizza}
				 		</Card.Title>
						<Card.Subtitle>Purchased On:</Card.Subtitle>
				   		<Card.Text>{purchasedOn}</Card.Text>
				  	</Card.Body>
				</Card>
			</Col>
		
		</Container>
		</Fragment>
	)
}

