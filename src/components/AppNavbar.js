import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

  const { user } = useContext(UserContext)

  return(
    <Navbar bg="light" variant="light" expand="lg">
        <Navbar.Brand className="mx-3" as={ Link } to="/">Pizzeria 157</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto mx-3">
                <Nav.Link  as={ Link } to="/">Home</Nav.Link>
                <Nav.Link as={ Link } to="/products">Pizza</Nav.Link>
                {
                    (user.id !== null) ?
                    <Fragment>
                        {(user.isAdmin === true) ? 
                            <Fragment>
                            <Nav.Link as={ Link } to="/addproducts">Add Pizza</Nav.Link>
                            <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                            </Fragment>
                            :
                            <Fragment>
                            <Nav.Link as={ Link } to="/cart">Cart</Nav.Link>
                            <Nav.Link as={ Link } to="/orders">Orders</Nav.Link>
                            <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                            </Fragment>
                } 
                    
                    </Fragment>
                    :
                    <Fragment>
                        <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                        <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                    </Fragment>
                }
            </Nav>
        </Navbar.Collapse>
    </Navbar>
    )
}
