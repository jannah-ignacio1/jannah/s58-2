import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}) {

	const {name, description, price, _id, stocks} = productProp

	return(
		
		<Card style={{ width: '25rem' }}>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		       <Card.Subtitle>Price:</Card.Subtitle>
		       <Card.Text>Php {price}</Card.Text>
		       <Card.Subtitle>Description:</Card.Subtitle>
		       <Card.Text>{description}</Card.Text>
		       <hr/>
		       <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>

	)
}
