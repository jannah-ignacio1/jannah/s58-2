import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Higlights() {
	
	const {user}=useContext(UserContext)

	return(

		user.isAdmin ?

		<Row className="mt-3 mb-3">
			
		</Row>
		:
		<Row className="mt-3 mb-3">

			<Col xs={12} md={4}>
				<Card className="highlight-card p-3">
					<Card.Body>
						<Card.Title>Lorem</Card.Title>
							<Card.Text>Lorem Ipsum
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="highlight-card p-3">
					<Card.Body>
						<Card.Title>Lorem</Card.Title>
							<Card.Text>Lorem Ipsum
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>
		)
}
