import React, { Fragment, useContext } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { useLocation, Link } from 'react-router-dom';
import UserContext from '../UserContext'

export default function Banner(){

	const { user } = useContext(UserContext)
	const url = useLocation()
	const path = url.pathname

	return(

			<Fragment>{
			(path === '/') ?
				(!user.isAdmin) ?
				<Container fluid>
					<Row>
						<Col className="p-5">
							<h1 className="banner-txt">Pizzeria 157</h1>
							<p className="banner-txt">Life is not about finding yourself. It's about finding pizza.</p>
							<Button variant="primary" as={ Link } to="/products">Order Now!</Button>
						</Col>
					</Row>
				</Container>
				:
				<Container fluid>
					<Row>
						<Col className="p-5">
							<h1 className="banner-txt">Pizzeria 157</h1>
							<p className="banner-txt">Hello, Admin!</p>
							<Button style={{width: '10rem'}} variant="primary" as={ Link } to="/products">Product Listing</Button><br />
							<br/>
							<Button style={{width: '10rem'}} variant="primary" as={ Link } to="/orders">Order Listing</Button>
						</Col>
					</Row>
				</Container>
			:
			<Fragment>
				<Container fluid>
					<Row>
						<Col className="p-5">
							<h1 className="banner-txt">Oopsie! Page Not Found</h1>
							<h5 className="banner-txt">You won't find your pizza here</h5>
							<p className="banner-txt">Go back to <Link as={ Link } to="/">homepage.</Link></p>
						</Col>
					</Row>
				</Container>
			</Fragment>
			}
			</Fragment>
		)

}
