import { useState } from 'react';
import { Button, Col, Card, Container } from 'react-bootstrap'
import checkout from '../pages/Cart';

export default function Carts({itemProp}) {
	const { name, quantity, price, subtotal, productId } = itemProp
	const [buttonQuantity, setButtonQuantity] = useState(quantity)
	const [newSubtotal, setNewSubtotal] = useState(subtotal)

	
	fetch('https://calm-dawn-93452.herokuapp.com/users/myCart', {
			headers: {
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
	.then(res => res.json())
	.then(data => {
		if(data.length === 0){
			localStorage.setItem('cartTotal', 0)
		} else {
			localStorage.setItem('cartTotal',  data.cartTotal)
		}
	})

const reduce = () => {
	fetch(`https://calm-dawn-93452.herokuapp.com/users/myCart/deleteProduct`, {
			method:'DELETE',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
	.then(res => res.json())
	.then(data => {
		setButtonQuantity(data.quantity)
		setNewSubtotal(data.subtotal)
			localStorage.setItem('subtotal', data.subtotal)
	})
}
	
const add = () => {
	fetch(`https://calm-dawn-93452.herokuapp.com/users/addToCart`, {
			method:'POST',
			headers:{
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
	.then(res => res.json())
	.then(data => {
		setButtonQuantity(quantity)
		setNewSubtotal(subtotal)
			localStorage.setItem('subtotal', data.subtotal)
	})
}

const remove=()=>{
	fetch(`https://calm-dawn-93452.herokuapp.com/users/myCart/removeProduct`, {
			method:'DELETE',
			headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}, 
			body: JSON.stringify({
				productId: productId
			})
		})
	.then(res => res.json())
	.then(data => {
		setButtonQuantity(quantity)
		setNewSubtotal(subtotal)
			localStorage.setItem('subtotal', data.subtotal)
	})
}

	return (
		<Container className="my-5 vh-50 d-inline-flex justify-content-center">
		
			<Col className="col-lg-5">
				<Card className='p-3'>
				 	<Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>quantity: {buttonQuantity}</Card.Subtitle>
						   	 	<Button variant="dark" size='sm' onClick={reduce}> - </Button>
						   		<Button variant="dark" size='sm' onClick={add}> + </Button>
						   	  	<Button variant="danger" size='sm' onClick={remove}> Remove </Button>
				  	</Card.Body>
				  	<Card.Body>
				  		<Card.Subtitle>Total Bill:</Card.Subtitle>
				   	 		<Card.Text>{newSubtotal}</Card.Text>
				   	 		<Button variant="primary" size='sm' onClick={checkout}>Checkout</Button>
				  	</Card.Body>
				</Card>				
			</Col>
			<Col className="text-center justify-content-center">
				
			</Col>
		</Container>
	)
}
