import { useState, useEffect } from 'react';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import { Form, Button, Row, Col, Container, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProducts(productProp){

	const {productId} = useParams()
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [stocks, setStocks] = useState(0)
	const [status, setStatus] = useState(false)
	const [newName, setNewName] = useState(name)
	const [newDescription, setNewDescription] = useState(description)
	const [newPrice, setNewPrice] = useState(price)
	const [newStocks, setNewStocks] = useState(stocks)
	const [newStatus, setNewStatus] = useState(status)
	const [isActive, setIsActive]=useState(false)
	const navigate = useNavigate()

	async function fetchData(){
		const data = await fetch(`https://calm-dawn-93452.herokuapp.com/products/${productId}`)
		const item = await data.json()
			return item
	}
	
	fetchData().then(item => {
		setName(item.name)
		setDescription(item.description)
		setPrice(item.price)
		setStocks(item.stocks)
		setStatus(item.isActive)
	})
	
	function edit(e){
		e.preventDefault()

		fetch(`https://calm-dawn-93452.herokuapp.com/products/${productId}`,{
			method:'PUT',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem("token")}`
			},
			body:JSON.stringify({
				name: newName,
				description: newDescription,
				price: newPrice,
				stocks: newStocks,
				isActive: newStatus
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
			 	Swal.fire({
					title:'Updated Successfully',
					icon:'success'
				})

				navigate('/products')
			
			} else {
			 	Swal.fire({
					title:'Failed',
					icon:'error',
					text:'Please try again'
				})
			}
		})
	}

	useEffect(()=>{
		if ( newName !== '' || newDescription !== '' || newPrice !== '' || status !== '' || newStocks !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [newName, newDescription, newPrice, newStocks, status])

	return(
		<Container className="vh-50 d-inline-flex justify-content-center">
				
					<Col className="col-lg-5">
						<Card className="p-5 login">
							<h2 className="text-center">Edit Product</h2>
								<Form onSubmit={e=>edit(e)} className="my-5">  
									
								  	<Form.Group className="mb-3" controlId="productName">
									    <Form.Label>Name</Form.Label>
									    	<Form.Control 
							    				type="text"
							    				className=""
							    				placeholder={name}
							    				value={newName}
							    				onChange= {e=>setNewName(e.target.value)}
							    				/>
								    		<Form.Text className="text-muted">
										    </Form.Text>
								  	</Form.Group>

								  	<Form.Group className="mb-3" controlId="productDesc">
								    	<Form.Label>Description</Form.Label><br />
								    		<textarea 
							    				className="col-12"
							    				placeholder={description}
							    				style={{height: '5rem'}}
							    				value={newDescription}
							    				onChange= {e=>setNewDescription(e.target.value)}
							    				 />	
								  	</Form.Group>

									<Form.Group className="mb-3" controlId="price">
									    <Form.Label>Price</Form.Label>
									    	<Form.Control 
							    				type="number" 
							    				className=""
							    				placeholder={price}
							    				value={newPrice}
							    				onChange= {e=>setNewPrice(e.target.value)}
							    				 />
									</Form.Group>

									<Form.Group className="mb-3" controlId="stocks">
									    <Form.Label>Stock</Form.Label>
									    	<Form.Control 
							    				type="number" 
							    				className=""
							    				placeholder={stocks}
							    				value={newStocks}
							    				onChange= {e=>setNewStocks(e.target.value)}
							    				 />
									</Form.Group>

								  	<Form.Group className="mb-3" controlId="productStatus">
								    	<Form.Label>Status</Form.Label>
								    		<Form.Control 
							    				type="text" 
							    				className=""
							    				placeholder="status"
							    				value={newStatus}
							    				onChange= {e=>setNewStatus(e.target.value)}
							    				 />
								  	</Form.Group>
								
										{
										  	isActive ?
										  	<Button variant="success" type="submit" id="submitBtn">
										  	 Save
										  	</Button>
										  	:
										  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
										  	 Save
										  	</Button>
										}
								</Form>
						</Card>
					</Col>
				
		</Container>
	)
		
	
}

