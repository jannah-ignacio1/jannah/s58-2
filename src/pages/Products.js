import PropTypes from 'prop-types';
import { Fragment, useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		if(user.isAdmin == true) {
			//get all products
			fetch('https://calm-dawn-93452.herokuapp.com/products/all', {
				method: 'POST',
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setProducts(data.map(product => {
					return(
						<ProductCard key={product._id} productProp={product} />)
				}))
			})
		} else {
			//active products
			fetch('https://calm-dawn-93452.herokuapp.com/products/')
			.then(res => res.json())
			.then(data => {
				setProducts(data.map(product => {
					return(
						<ProductCard key={product._id} productProp={product} />)
				}))
			})
		}
	}, [])

	return(

		<Container className="">
			<div className="d-flex flex-row col-12">
			
				{products}
			
			</div>
		</Container>
		
		)
}


ProductCard.propTypes = {

	productProp: PropTypes.shape({
		code: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stocks: PropTypes.number.isRequired
	})
}
