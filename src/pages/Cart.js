import { Fragment, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Row, Col, Card } from 'react-bootstrap';
import CartCard from '../components/CartCard';
import Swal from 'sweetalert2';

export default function Cart(){
	const [items, setItems] = useState([])
	const [subtotal, setSubtotal] = useState(0)
	const navigate = useNavigate()
	
	const checkout = () => {
		fetch('https://calm-dawn-93452.herokuapp.com/users/checkout', {
			method:'POST',
			headers:{
				Authorization:`Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if((data !== undefined || data !== ' ') && data.products.length > 0) {
				Swal.fire({
					title:'Thank you for your order',
					icon:'success',
					text: 'Your Pizza is now cooking!'
				})
				navigate('/')

			} else if ((data === undefined || data === ' ') && data.products.length < 0){
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text:'Please try again'
				})
			}
		})
	}

	useEffect(()=>{
		fetch('https://calm-dawn-93452.herokuapp.com/users/myCart', {
			headers:{
				Authorization:`Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setSubtotal(localStorage.getItem('cartTotal'))
			setItems(data.products.map(item => {
				return (
					<CartCard key={ item.productId } itemProp={ item }/>					
					)
			}))
		})

	},[items])


	return(

		<Fragment>
			{ items }
		
		</Fragment>
	)
}