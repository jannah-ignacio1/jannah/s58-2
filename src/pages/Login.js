import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate()

	function loginUser(e) {

		e.preventDefault()

		fetch('https://calm-dawn-93452.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to Pizzeria"
			})

			navigate("/")

			} else {
				Swal.fire({
					title: "Sorry! Login Failed",
					icon: "error",
					text: "Check your inputs and try again!"
				})
			}
		})

	setEmail('');
	setPassword('');
	}

	const retrieveUserDetails = (token) => {

		fetch('https://calm-dawn-93452.herokuapp.com/users/', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if((email !== '' && password !=='')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(

		(user.id !== null) ?
			<Navigate to="/products" />
			
			:
			<Container className="vh-50 d-inline-flex justify-content-center" >
			<Form onSubmit = {(e) => loginUser(e)}>
				<Form.Group className="mb-3" controlId="userEmail">
					<h1>Login</h1>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
								type 		= "email"
								placeholder = "Enter email address"
								value 		= {email}
								onChange 	= {e=>setEmail(e.target.value)}
								required/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control 
								type 		= "password"
								placeholder = "Enter password"
								value 		= {password}
								onChange 	= {e=>setPassword(e.target.value)}
								required/>
				</Form.Group>
			{
			isActive ?

				<Button variant="primary" type="login" id="login-btn">Login</Button>
				:
				<Button variant="danger" type="login" id="login-btn" disabled>Login</Button>
			}

			</Form>
		</Container>	
		)
}
