import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Card, Row, Col, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const quantity = 1;

	const addToCart = (productId) => {
		
		fetch(`https://calm-dawn-93452.herokuapp.com/users/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			if(data !== undefined){
				Swal.fire({
					title: "Added to Cart",
					icon: "success"
				})

			navigate("/cart")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error"
				})
			}
		})
	}

	useEffect(() => {

		fetch(`https://calm-dawn-93452.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	return(
		
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset: 3}}>
					<Card>
						<Card.Body className="">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{ description }</Card.Text>
							{(user.id !== null) ?
								(user.isAdmin) ?
								<Fragment>
									<Link className="btn btn-primary" to={`/products/${productId}/editproduct`}>Edit</Link>
								</Fragment>
								:
								<Fragment>

								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
									<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
								</Fragment>
							: 
							<Fragment>
							<Link className="btn btn-danger" to="/login">Login to Order</Link>
							</Fragment>

							}
								
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)	
}
