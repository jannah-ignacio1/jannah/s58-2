import { Form, Button, Row, Col, Container, Card} from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import { useState, useEffect, useContext } from 'react'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function AddProduct(){

	const { user } = useContext(UserContext)
	const [name,setName] = useState('')
	const [description,setDescription] = useState('')
	const [price,setPrice] = useState(0)
	const [stocks, setStocks] = useState(0)
	const [isActive,setIsActive] = useState(false)
	const navigate = useNavigate()

	function add(e){
		e.preventDefault()

		fetch(`https://calm-dawn-93452.herokuapp.com/products/`,{
			method:'POST',
			headers:{
				'Content-type':'application/json',
				Authorization:`Bearer ${localStorage.getItem("token")}`
			},
			body:JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks			
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if (typeof data !== "undefined"){
				Swal.fire({
					title:'Product added successfully',
					icon:'success'
				})
			//setName('');
			//setDescription('');
			//setPrice('');
			//setStocks('');

			navigate("/products")

			} else {
				Swal.fire({
					title:'Try Again',
					icon:'error',
				})
			}
		})
	}	

	useEffect(()=>{
		if (name !== '' && description !== ''&& price !== '' && stocks !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name, description, price, stocks])

	return(
		(user.isAdmin == false) ?
		<Navigate to='/'/>
		:
		<Container className="my-5 vh-50 d-inline-flex justify-content-center">
				
					<Col className="col-lg-5">
						<Card className="p-5 loginCard">
							<h2 className="text-center">Add Product</h2>
								<Form onSubmit={e=>add(e)} className="my-5">
										  
										  <Form.Group className="mb-3" controlId="productName">
										    <Form.Label>Name</Form.Label>
										    <Form.Control 
										    				type="text"
										    				className=""
										    				placeholder="name"
										    				value={name}
										    				onChange= {e=>setName(e.target.value)}
										    				/>
										    <Form.Text className="text-muted">
										    
										    </Form.Text>
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="productDesc">
										    <Form.Label>Description</Form.Label><br />
										    
										    <textarea 
										    				type="text" 
										    				className="col-12"
										    				style={{height: '5rem'}}
										    				placeholder="description"
										    				value={description}
										    				onChange= {e=>setDescription(e.target.value)}
										    				 />
										    
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="price">
										    <Form.Label>Price</Form.Label>
										    <Form.Control 
										    				type="number" 
										    				className=""
										    				placeholder="price"
										    				value={price}
										    				onChange= {e=>setPrice(e.target.value)}
										    				 />
										  </Form.Group>

										  <Form.Group className="mb-3" controlId="stocks`">
										    <Form.Label>Stocks</Form.Label>
										    <Form.Control 
										    				type="number" 
										    				className=""
										    				placeholder="stocks"
										    				value={stocks}
										    				onChange= {e=>setStocks(e.target.value)}
										    				 />
										  </Form.Group>
										
										{
										  	isActive ?
										  	<Button variant="success" type="submit" id="submitBtn">
										  	  Add
										  	</Button>
										  	:
										  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
										  	 Add
										  	</Button>
										}
								</Form>
						</Card>
					</Col>
				
		</Container>
	)
}
