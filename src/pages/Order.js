import { Fragment, useEffect, useState, useContext } from 'react';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';

export default function Orders(){

	const { user } = useContext(UserContext)
	const [orders, setOrders] = useState([]);

	useEffect(() => {
		if(user.isAdmin == true) {
			//get all orders
			fetch('https://calm-dawn-93452.herokuapp.com/users/orders', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setOrders(data.map(order => {
					return(
						<OrderCard key={order._id} orderProp={order} />)
				}))
			})
		} else {
			//order of logged in user
			fetch('https://calm-dawn-93452.herokuapp.com/users/myOrders', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setOrders(data.map(order => {
					return(
						<OrderCard key={order._id} orderProp={order} />)
				}))
			})
		}
	}, [])

	return(

		<Fragment>
			{orders}
		</Fragment>
		
		)
}