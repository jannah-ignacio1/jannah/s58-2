import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Container, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Registration() {

	const {user, setUser} = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();


	function registerUser(e) {

		e.preventDefault()

		fetch('https://calm-dawn-93452.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data == true) {
				Swal.fire({
					title: "Email already exists",
					icon: "error",
					text: "Provide another email address."
				})
			} else {
				fetch('https://calm-dawn-93452.herokuapp.com/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data == true){
						Swal.fire({
							title: "Registration Complete",
							icon: "success",
							text: "You may now login."
						})

						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword1('');
						setMobileNo('');

						navigate("/login")

					} else {
						Swal.fire({
							title: "Registration Failed",
							icon: "error",
							text: "Check your inputs and try again."
						})
					}
				})
			}				
		})
	}
	
	useEffect(() => {

		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !== '') && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, mobileNo, password2])

		return(

			(user.id !== null) ?

				<Navigate to="/products" />

				:
				<Container className="vh-50 d-inline-flex justify-content-center">
				<Col className="col-lg-5">
				<Form className="mt-3" onSubmit = {(e) => registerUser(e)}>

					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control
							type		= "text"
							placeholder	= "Enter first name"
							value		= {firstName}
							onChange	= {e => setFirstName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control
							type		= "text"
							placeholder	= "Enter last name"
							value		= {lastName}
							onChange	= {e => setLastName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control 
							type		= "email" 
							placeholder	= "Enter valid email address"
							value 		= {email}
							onChange 	= {e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Cellphone Number</Form.Label>
						<Form.Control
							type		= "text"
							placeholder	= "Enter valid cellphone number"
							value		= {mobileNo}
							onChange	= {e => setMobileNo(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type		= "password" 
							placeholder	= "Password"
							value		= {password1}
							onChange	= {e => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="password2">
						<Form.Label> Verify Password</Form.Label>
						<Form.Control 
							type		= "password" 
							placeholder	= "Verify Password"
							value		= {password2}
							onChange	= {e=> setPassword2(e.target.value)}
							required
						/>
					</Form.Group>
				{
					setIsActive ?

						<Button variant="primary" type="submit" id="submit-btn">Submit</Button>
						:
						<Button variant="danger" type="submit" id="submit-btn" disabled>Submit</Button>
				}
				</Form>
				</Col>
				</Container>
	)
}
