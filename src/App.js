import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import AppNavbar from './components/AppNavbar';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import Order from './pages/Order';

function App() {

	const [user, setUser] = useState({
		id: null,
		password: null
	})

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		})
	}

	useEffect(() => {
		fetch('https://calm-dawn-93452.herokuapp.com/users/', {
			//route from API for retrieving user details
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(typeof data._id !== "undefined") {
				setUser({
					id: data.id,
					isAdmin: data.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])

	return (


		<UserProvider value = {{user, setUser, unsetUser}}>
			<Router>
				<AppNavbar />
				<Container>
					<Routes>
						<Route exact path="/" element={<Home/>} />
						<Route exact path="/home" element={<Home/>} />
						<Route exact path="/register" element={<Register/>} />
						<Route exact path="/login" element={<Login/>} />
						<Route exact path="/logout" element={<Logout/>} />
						<Route exact path="/products" element={<Products/>} />
						<Route exact path="/products/:productId" element={<ProductView/>} />
						<Route exact path="/addproducts" element={<AddProduct/>} />
						<Route exact path="/products/:productId/editproduct" element={<EditProduct/>} />
						<Route exact path="/cart" element={<Cart/>} />
						<Route exact path="/orders" element={<Order/>} />
						<Route exact path="*" element={<NotFound/>} />
					</Routes>
				</Container>
			</Router>
		</UserProvider>
		);
}

export default App;
